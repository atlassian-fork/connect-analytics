// generated on 2017-02-24 using generator-chrome-extension 0.6.1
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import del from 'del';
import runSequence from 'run-sequence';
import { stream as wiredep } from 'wiredep';
import webpack from 'webpack-stream';

const $ = gulpLoadPlugins();

gulp.task('extras', () => gulp.src([
  'app/*.*',
  'app/_locales/**',
  '!app/scripts.babel',
  '!app/*.json',
  '!app/*.html',
], {
  base: 'app',
  dot: true,
}).pipe(gulp.dest('dist')));

function lint(files, options) {
  return () => gulp.src(files)
      .pipe($.eslint(options))
      .pipe($.eslint.format());
}

gulp.task('lint', lint('app/scripts.babel/**/*.js'));

gulp.task('images', () => gulp.src('app/images/**/*')
    .pipe($.if($.if.isFile, $.cache($.imagemin({
      progressive: true,
      interlaced: true,
      // don't remove IDs from SVGs, they are often used
      // as hooks for embedding and styling
      svgoPlugins: [{ cleanupIDs: false }],
    }))
    .on('error', function (err) {
      console.log(err);
      this.end();
    })))
    .pipe(gulp.dest('dist/images')));

gulp.task('html', () => gulp.src('app/*.html')
    .pipe($.useref({ searchPath: ['.tmp', 'app', '.'] }))
    .pipe($.sourcemaps.init())
    .pipe($.if('*.js', $.uglify()))
    .pipe($.if('*.css', $.cleanCss({ compatibility: '*' })))
    .pipe($.sourcemaps.write())
    .pipe($.if('*.html', $.htmlmin({ removeComments: true, collapseWhitespace: true })))
    .pipe(gulp.dest('dist')));

gulp.task('chromeManifest', () => gulp.src('app/manifest.json')
    .pipe($.chromeManifest({
      buildnumber: true,
      background: {
        target: 'scripts/background.js',
        exclude: [
          'scripts/chromereload.js',
        ],
      },
    }))
  .pipe($.if('*.css', $.cleanCss({ compatibility: '*' })))
  .pipe($.if('*.js', $.sourcemaps.init()))
  .pipe($.if('*.js', $.uglify()))
  .pipe($.if('*.js', $.sourcemaps.write('.')))
  .pipe(gulp.dest('dist')));

gulp.task('webpack', () => gulp.src('src/entry.js')
    .pipe(webpack({
      module: {
        loaders: [
          {
            test: /\.js/,
            // No exclude
            loader: 'babel-loader',
            query: {
              presets: ['es2015'],
            },
          },
        ],
      },
      entry: {
        background: './app/scripts.babel/background.js',
        chromereload: './app/scripts.babel/chromereload.js',
        main: './app/scripts.babel/main.js',
      },
      output: {
        filename: '[name].js',
      },
    }))
    .pipe(gulp.dest('app/scripts/')));

gulp.task('clean', del.bind(null, ['.tmp', 'dist']));

gulp.task('watch', ['lint', 'webpack'], () => {
  $.livereload.listen();

  gulp.watch([
    'app/*.html',
    'app/scripts/**/*.js',
    'app/images/**/*',
    'app/styles/**/*',
    'app/_locales/**/*.json',
  ]).on('change', $.livereload.reload);

  gulp.watch('app/scripts.babel/**/*.js', ['lint', 'webpack']);
  gulp.watch('bower.json', ['wiredep']);
});

gulp.task('size', () => gulp.src('dist/**/*').pipe($.size({ title: 'build', gzip: true })));

gulp.task('wiredep', () => {
  gulp.src('app/*.html')
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)*\.\./,
    }))
    .pipe(gulp.dest('app'));
});

gulp.task('package', () => {
  const manifest = require('./dist/manifest.json');
  return gulp.src('dist/**')
      .pipe($.zip(`dist-${manifest.version}.zip`))
      .pipe(gulp.dest('package'));
});

gulp.task('build', (cb) => {
  runSequence(
    'webpack', 'chromeManifest',
    ['html', 'images', 'extras'],
    'size', cb);
});

gulp.task('default', ['clean'], (cb) => {
  runSequence('build', cb);
});
